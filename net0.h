#ifndef NET0_H
#define NET0_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifndef NET0_FRAME_BUFFERS_COUNT
#define NET0_FRAME_BUFFERS_COUNT (1)
#endif
typedef volatile struct net0frameBuff NET0_stream_t;

size_t NET0_pack(const uint8_t *restrict dataIn, uint8_t *restrict dataOut, 
                 size_t sizeIn, size_t sizeOut);
size_t NET0_unpack(const uint8_t *restrict dataIn, uint8_t *restrict dataOut, 
                   size_t sizeIn, size_t sizeOut);

size_t NET0_streamAddByte(NET0_stream_t *stream, uint8_t newByte);
NET0_stream_t *NET0_newStream(uint8_t *data, size_t size);
void NET0_clearStream(NET0_stream_t *stream);
bool NET0_streamComplete(const NET0_stream_t *stream);
size_t NET0_streamSize(NET0_stream_t *stream);
size_t NET0_streamPackedSize(NET0_stream_t *stream);

#ifdef TEST
void NET0_deinit(void);
#endif

#endif

