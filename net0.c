#include "net0.h"
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#ifdef TEST
#include <stdio.h>
#include <string.h>
#endif

#ifndef NELEMS
#define NELEMS(a) (sizeof(a) / sizeof(a[0]))
#endif

typedef uint8_t specialByte_t;

struct net0frameBuff
{
    uint8_t *data;
    size_t size;
    size_t position;
    size_t packedSize;
    bool stxReceived;
    bool lastByteWasDLE;
    bool etxReceived;
};

static const specialByte_t NET0_STX = 0x02;
static const specialByte_t NET0_DLE = 0x10;
static const specialByte_t NET0_ETX = 0x03;
static volatile struct net0frameBuff frameBuffers[NET0_FRAME_BUFFERS_COUNT];

size_t NET0_pack(const uint8_t *restrict dataIn, uint8_t *restrict dataOut,
                 size_t sizeIn, size_t sizeOut)
{
    if(dataIn == NULL || dataOut == NULL
       || dataIn == dataOut
       || sizeIn == 0 || sizeOut == 0)
    {
        return 0;
    }

    dataOut[0] = NET0_STX;
    size_t outIndex = 1;

    for(size_t i = 0; i < sizeIn; i++)
    {
        if(outIndex >= sizeOut)
        {
            return 0;
        }

        if(dataIn[i] == NET0_STX
           || dataIn[i] == NET0_DLE
           || dataIn[i] == NET0_ETX)
        {
            dataOut[outIndex++] = NET0_DLE;

            if(outIndex >= sizeOut)
            {
                return 0;
            }

            dataOut[outIndex++] = dataIn[i] | 0x80;
        }
        else
        {
            dataOut[outIndex++] = dataIn[i];
        }
    }

    if(outIndex < sizeOut)
    {
        dataOut[outIndex] = NET0_ETX;
    }
    else
    {
        return 0;
    }

    return outIndex + 1;
}

void NET0_deinit(void)
{
    memset((void *)frameBuffers, 0, sizeof(frameBuffers));
}

size_t NET0_unpack(const uint8_t *restrict dataIn, uint8_t *restrict dataOut,
                   size_t sizeIn, size_t sizeOut)
{
    if(dataIn == NULL || dataOut == NULL
       || dataIn == dataOut
       || sizeIn < (sizeof(NET0_STX) + sizeof(NET0_ETX) + sizeof(uint8_t))
       || dataIn[0] != NET0_STX || dataIn[sizeIn - 1] != NET0_ETX)
    {
        return 0;
    }

    size_t outIndex = 0;
    sizeIn -= sizeof(NET0_ETX);

    for(size_t i = sizeof(NET0_STX); i < sizeIn; i++)
    {
        if(outIndex >= sizeOut)
        {
            return 0;
        }

        if(dataIn[i] == NET0_DLE)
        {
            if(i + 1 >= sizeIn)
            {
                return 0;
            }

            dataOut[outIndex++] = dataIn[++i] & (uint8_t)~0x80;
        }
        else
        {
            dataOut[outIndex++] = dataIn[i];
        }
    }

    return outIndex;
}

volatile struct net0frameBuff *NET0_newStream(uint8_t *data, size_t size)
{
    if(data == NULL || size == 0)
    {
        return NULL;
    }

    for(size_t i = 0; i < NELEMS(frameBuffers); i++)
    {
        if(frameBuffers[i].size == 0)
        {
            frameBuffers[i].size = size;
            frameBuffers[i].data = data;
            frameBuffers[i].etxReceived = false;
            return &frameBuffers[i];
        }
        else if(frameBuffers[i].data == data)
        {
            return NULL;
        }
    }

    return NULL;
}

size_t NET0_streamAddByte(NET0_stream_t *stream, uint8_t newByte)
{
    if(stream == NULL || stream->etxReceived)
    {
        return 0;
    }

    ++stream->packedSize;
    if(newByte == NET0_STX)
    {
        stream->position = 0;
        stream->packedSize = 1;
        stream->stxReceived = true;
        stream->lastByteWasDLE = false;
        return 0;
    }
    else if(!stream->stxReceived)
    {
        return 0;
    }

    if(newByte == NET0_ETX && stream->position > 0)
    {
        stream->etxReceived = true;
        return stream->position;
    }
    else
    {
        if(newByte == NET0_DLE)
        {
            if(stream->lastByteWasDLE)
            {
                stream->stxReceived = false;
                return 0;
            }

            stream->lastByteWasDLE = true;
        }
        else if(stream->position < stream->size)
        {
            if(stream->lastByteWasDLE)
            {
                stream->lastByteWasDLE = false;
                newByte &= (uint8_t)~(0x80);
            }

            stream->data[stream->position++] = newByte;
        }
        else
        {
            stream->stxReceived = false;
            return 0;
        }
    }

    return 0;
}

void NET0_clearStream(NET0_stream_t *stream)
{
    if(stream != NULL)
    {
        stream->stxReceived = false;
        stream->etxReceived = false;
    }
}

bool NET0_streamComplete(const NET0_stream_t *stream)
{
    return stream != NULL && stream->stxReceived && stream->etxReceived;
}

size_t NET0_streamSize(NET0_stream_t *stream)
{
    if(NET0_streamComplete(stream))
    {
        return stream->position;
    }
    return 0;
}

size_t NET0_streamPackedSize(NET0_stream_t *stream)
{
    if(NET0_streamComplete(stream))
    {
        return stream->packedSize;
    }
    return 0;
}
