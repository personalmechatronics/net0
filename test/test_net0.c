#include "net0.h"
#include "unity.h"
#include <stddef.h>

#ifndef NELEMS 
#define NELEMS(a) (sizeof(a) / sizeof(a[0]))
#endif

//+-----------+
//| NET0_pack |
//+===========+

// cppcheck-suppress unusedFunction 
void test_NET0_pack_NullDataIn_should_ReturnZero(void)
{
    uint8_t dataOut[3]; 
    TEST_ASSERT_EQUAL(0, NET0_pack(NULL, dataOut, 3, sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_NullDataOut_should_ReturnZero(void)
{
    uint8_t dataIn[3]; 
    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, NULL, sizeof(dataIn), 3));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_ZeroSizeIn_should_ReturnZero(void)
{
    uint8_t dataIn[3] = {1, 2, 3}; 
    uint8_t dataOut[10]; 
    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, 0, sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_ZeroSizeOut_should_ReturnZero(void)
{
    uint8_t dataIn[3] = {1, 2, 3}; 
    uint8_t dataOut[10]; 
    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, sizeof(dataIn), 0));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_SamePtrToData_should_ReturnZero(void)
{
    uint8_t dataIn[10] = {1, 2, 3, 0, 0, 0, 0, 0, 0, 0}; 
    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataIn, 3, sizeof(dataIn)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingSingleByteWithoutDLE(void)
{
    uint8_t dataIn = 0xAA;
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0x02, 0xAA, 0x03};

    TEST_ASSERT_EQUAL(sizeof(expectedDataOut), NET0_pack(&dataIn, dataOut, 
                                                sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingSingleByteEqualToSTX(void)
{
    uint8_t dataIn = 0x02;
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0x02, 0x10, 0x82, 0x03};

    TEST_ASSERT_EQUAL(sizeof(expectedDataOut), NET0_pack(&dataIn, dataOut, 
                                                sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingSingleByteEqualToDLE(void)
{
    uint8_t dataIn = 0x10;
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0x02, 0x10, 0x90, 0x03};

    TEST_ASSERT_EQUAL(sizeof(expectedDataOut), NET0_pack(&dataIn, dataOut, 
                                                sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingSingleByteEqualToETX(void)
{
    uint8_t dataIn = 0x03;
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0x02, 0x10, 0x83, 0x03};

    TEST_ASSERT_EQUAL(sizeof(expectedDataOut), NET0_pack(&dataIn, dataOut, 
                                                sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingSingleByteDataOut_should_ReturnZeroAndStopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 0xAA};
    uint8_t dataOut[10] = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    uint8_t expectedDataOut[10] = {2, 12, 13, 14, 15, 16, 17, 18, 19, 20};

    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 1));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingToSmallDataOut_should_ReturnZeroAndStopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0xAA, 0xAA, 5, 6, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 4));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingToSmallDataOutAndLastByteAsDLE_should_ReturnZeroAndStopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 2};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0xAA, 0x10, 5, 6, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 4));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}
// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingToSmallBy2BytesDataOut_should_ReturnZeroAndStopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0xAA, 4, 5, 6, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(0, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 3));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingExactSizeDataOutWithoutDLE_should_StopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0xAA, 0xAA, 3, 6, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(5, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 5));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingExactSizeDataOutWithDLE_should_StopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0x10, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0x10, 0x90, 0xAA, 3, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(6, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), 6));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingBiggerDataOutWithoutDLE_should_StopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0xAA, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0xAA, 0xAA, 3, 6, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(5, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), sizeof(dataOut)));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PassingBiggerDataOutWithDLE_should_StopAtEndOfDataOut(void)
{
    uint8_t dataIn[] = {0xAA, 0x02, 0xAA};
    uint8_t dataOut[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t expectedDataOut[10] = {2, 0xAA, 0x10, 0x82, 0xAA, 3, 7, 8, 9, 10};

    TEST_ASSERT_EQUAL(6, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), sizeof(dataOut)));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingWithMultipleDLE(void)
{
    uint8_t dataIn[] = {0x0A, 0x02, 0x0C, 0x03, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13,
                        11, 12, 13};
    uint8_t dataOut[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 
                         13, 14, 15, 16, 17, 18, 19, 20};
    uint8_t expectedDataOut[] = {/*packed*/
                                0x02, 0x0A, 0x10, 0x82, 0x0C, 0x10, 0x83, 0x0E, 
                                 0x0F, 0x10, 0x90, 0x11, 0x12, 0x13, 11, 12, 13, 
                                 0x03/*end of packed*/, 19, 20};

    TEST_ASSERT_EQUAL(18, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), sizeof(dataOut)));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_pack_PackingDoesntModifyInput(void)
{
    uint8_t dataIn[] = {0x0A, 0x02, 0x0C, 0x03, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13,
                        11, 12, 13};
    uint8_t expectedDataIn[] = {0x0A, 0x02, 0x0C, 0x03, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13,
                        11, 12, 13};
    uint8_t dataOut[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 
                         13, 14, 15, 16, 17, 18, 19, 20};
    uint8_t expectedDataOut[] = {/*packed*/
                                0x02, 0x0A, 0x10, 0x82, 0x0C, 0x10, 0x83, 0x0E, 
                                 0x0F, 0x10, 0x90, 0x11, 0x12, 0x13, 11, 12, 13, 
                                 0x03/*end of packed*/, 19, 20};

    TEST_ASSERT_EQUAL(18, NET0_pack(dataIn, dataOut, 
                                   sizeof(dataIn), sizeof(dataOut)));
    //check pack did not modified data beyoind given output size
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataIn, dataIn, sizeof(expectedDataIn));
}
//+-------------+
//| NET0_unpack |
//+=============+

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_NullDataIn_should_ReturnZero(void)
{
    uint8_t dataOut[3]; 
    TEST_ASSERT_EQUAL(0, NET0_unpack(NULL, dataOut, 3, sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_NullDataOut_should_ReturnZero(void)
{
    uint8_t dataIn[3]; 
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, NULL, sizeof(dataIn), 3));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_ZeroSizeIn_should_ReturnZero(void)
{
    uint8_t dataIn[3] = {2, 2, 3}; 
    uint8_t dataOut[10]; 
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 0, sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_ZeroSizeOut_should_ReturnZero(void)
{
    uint8_t dataIn[3] = {2, 2, 3}; 
    uint8_t dataOut[10]; 
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, sizeof(dataIn), 0));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_SamePtrToData_should_ReturnZero(void)
{
    uint8_t dataIn[10] = {2, 1, 3, 0, 0, 0, 0, 0, 0, 0}; 
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataIn, 3, sizeof(dataIn)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_NoSTX_should_ReturnZero(void)
{
    uint8_t dataIn[] = {1, 1, 3};
    uint8_t dataOut[10];
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_NoETX_should_ReturnZero(void)
{
    uint8_t dataIn[] = {2, 1, 1};
    uint8_t dataOut[10];
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_NoSTXandNoETX_should_ReturnZero(void)
{
    uint8_t dataIn[] = {1, 1, 1};
    uint8_t dataOut[10];
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_OnlySTX_shouldReturnZeroAndNotModifyOut(void)
{
    uint8_t dataIn[] = {2};
    uint8_t dataOut[3] = {0xAA, 0xAA, 0xAA};
    uint8_t expectedDataOut[3] = {0xAA, 0xAA, 0xAA};
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(dataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_OnlySTX_DLE_ETX_shouldReturnZeroAndNotModifyOut(void)
{
    uint8_t dataIn[] = {0x02, 0x10, 0x03};
    uint8_t dataOut[3] = {0xAA, 0xAA, 0xAA};
    uint8_t expectedDataOut[3] = {0xAA, 0xAA, 0xAA};
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(dataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_DLEwithoutFollowingData_should_ReturnZero(void)
{
    uint8_t dataIn[] = {2, 0x10, 3};
    uint8_t dataOut[10];
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_SingleByteWithoutDLE_should_ReturnOneAndUnpack(void)
{
    uint8_t dataIn[] = {2, 0xBB, 3};
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0xBB};
    TEST_ASSERT_EQUAL(1, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_SingleByteWithDLE_should_ReturnOneAndUnpack(void)
{
    uint8_t dataIn[] = {2, 0x10, 0x90, 3};
    uint8_t dataOut[10];
    uint8_t expectedDataOut[] = {0x10};
    TEST_ASSERT_EQUAL(1, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_AllPossibleDLE_shouldReturnThreeAndUnpack(void)
{
    uint8_t dataIn[] = {2, 0x10, 0x82, 0x10, 0x90, 0x10, 0x83, 3};
    uint8_t dataOut[10] = {0};
    uint8_t expectedDataOut[] = {0x02, 0x10, 0x03};
    TEST_ASSERT_EQUAL(3, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_CorrectUnpacking_should_NotWriteMoreThanRequired(void)
{
    uint8_t dataIn[] = {2, 0x10, 0x82, 0x10, 0x90, 0x10, 0x83, 3};
    uint8_t dataOut[10] = {0, 0, 0, 1, 2, 3, 4, 5, 6, 7};
    uint8_t expectedDataOut[10] = {0x02, 0x10, 0x03, 1, 2, 3, 4, 5, 6, 7};
    TEST_ASSERT_EQUAL(3, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), sizeof(dataOut)));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_ToSmallOutSize_should_ReturnZeroAndNotWriteMore(void)
{
    uint8_t dataIn[] = {2, 0x10, 0x82, 0x10, 0x90, 0x10, 0x83, 3};
    uint8_t dataOut[10] = {0, 0, 0, 1, 2, 3, 4, 5, 6, 7};
    uint8_t expectedDataOut[10] = {0x02, 0x10, 0, 1, 2, 3, 4, 5, 6, 7};
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), 2));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

// cppcheck-suppress unusedFunction 
void test_NET0_unpack_ToSmallOutSizeBy2Bytes_should_ReturnZeroAndNotWriteMore(void)
{
    uint8_t dataIn[] = {2, 0x10, 0x82, 0x10, 0x90, 0x10, 0x83, 4,  3};
    uint8_t dataOut[10] = {0, 0, 0, 0, 2, 3, 4, 5, 6, 7};
    uint8_t expectedDataOut[10] = {0x02, 0x10, 0, 0, 2, 3, 4, 5, 6, 7};
    TEST_ASSERT_EQUAL(0, NET0_unpack(dataIn, dataOut, 
                                     sizeof(dataIn), 2));
    TEST_ASSERT_EQUAL_MEMORY(expectedDataOut, dataOut, sizeof(expectedDataOut));
}

//+-----------------------------+
//| NET0_streamBuff manipulation |
//+=============================+

// cppcheck-suppress unusedFunction 
void test_NET0_newStreame_NullData_should_ReturnNull(void)
{
    NET0_stream_t *stream = NET0_newStream(NULL, 10);
    TEST_ASSERT_NULL(stream);
}

// cppcheck-suppress unusedFunction 
void test_NET0_newStream_ZeroSize_should_ReturnNull(void)
{
    uint8_t data[10];
    NET0_stream_t *stream = NET0_newStream(data, 0);
    TEST_ASSERT_NULL(stream);
}

// cppcheck-suppress unusedFunction 
void test_NET0_newStream_ValidInput_should_ReturnBuffer(void)
{
    uint8_t data[10][10];
    for(size_t i = 0; i < NET0_FRAME_BUFFERS_COUNT; i++)
    {
        NET0_stream_t *stream = NET0_newStream(data[i], 10);
        TEST_ASSERT_NOT_NULL(stream);
    }
}

// cppcheck-suppress unusedFunction 
void test_NET0_newStream_TooManyInits_should_ReturnNULL(void)
{
    uint8_t data[10][10];
    for(size_t i = 0; i < NET0_FRAME_BUFFERS_COUNT; i++)
    {
        NET0_stream_t *stream = NET0_newStream(data[i], 10);
        TEST_ASSERT_NOT_NULL(stream);
    }
    NET0_stream_t *stream = NET0_newStream(data[NET0_FRAME_BUFFERS_COUNT], 10);
    TEST_ASSERT_NULL(stream);
}

// cppcheck-suppress unusedFunction 
void test_NET0_newStream_InitializingWithUsedPtr_should_ReturnNULL(void)
{
    uint8_t data[10];

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    NET0_stream_t *stream2 = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NULL(stream2);
}

//+--------------------+
//| NET0_streamAddByte |
//+====================+

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingNullStream_should_ReturnZero(void)
{
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(NULL, 0x02));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingSTX_shouldReturnZero(void)
{
    uint8_t data[10];

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingETX_shouldReturnZero(void)
{
    uint8_t data[10];

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingDLE_shouldReturnZero(void)
{
    uint8_t data[10];

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingSTXAndData_shouldReturnZero(void)
{
    uint8_t data[10];

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x11));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingOneByteFrameWithoutDLE_shouldReturnOneAndValidData(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x11));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));

    TEST_ASSERT_EQUAL(0x11, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingOneByteFrameWithDLE_shouldReturnOneAndValidData(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));

    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingSTXETX_shouldReturnZero(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingFrameWithoutSTX_shouldReturnZero(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x04));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingFrameWithDLEWithoutSTX_shouldReturnZero(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingTwoDLE_should_ReturnZeroAndResetFrame(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x05));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingSTXDuringFrame_should_ReturnZeroAndClearFrame(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x05));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_PassingOnlySTXETX_should_ReturnZeroAndClearFrame(void)
{
    uint8_t data[10] = {0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_BufferSizeTooSmall_should_ReturnZeroAndClearFrame(void)
{
    uint8_t data[10] = {0};
    uint8_t expectedData[10] = {2, 4, 0, 0, 0, 0, 0, 0, 0, 0};

    NET0_stream_t *stream = NET0_newStream(data, 2);
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x04));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x05));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL_MEMORY(expectedData, data, sizeof(expectedData));
    //should return to processing next correct stream
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL(0x02, data[0]);
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_BufferOfSizeOne_should_ReturnOne(void)
{
    uint8_t data[10] = {0};
    uint8_t expectedData[10] = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    NET0_stream_t *stream = NET0_newStream(data, 1);
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(1, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL_MEMORY(expectedData, data, sizeof(expectedData));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_CompleteFrame_should_BeUmodifiableUntilClear(void)
{
    uint8_t data[10] = {0};
    uint8_t expectedData[10] = {2, 4, 0, 0, 0, 0, 0, 0, 0, 0};

    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_NOT_NULL(stream);

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x10));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x82));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x04));
    TEST_ASSERT_EQUAL(2, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL_MEMORY(expectedData, data, sizeof(expectedData));
    TEST_ASSERT_TRUE(NET0_streamComplete(stream));

    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x02));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x01));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x05));
    TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, 0x03));
    TEST_ASSERT_EQUAL_MEMORY(expectedData, data, sizeof(expectedData));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamAddByte_Passing1000Bytes_shouldReturn1000AndData(void)
{
    uint8_t data[1000];
    uint8_t packedData[2 * sizeof(data)];
    uint8_t buffer[2 * sizeof(data)] = {0};

    for(size_t i = 0; i < NELEMS(data); i++)
    {
        data[i] = (uint8_t)i;
    }
    
    size_t packedSize = NET0_pack(data, packedData, 
                                  sizeof(data), sizeof(packedData));
    TEST_ASSERT_GREATER_OR_EQUAL(sizeof(data), packedSize);

    NET0_stream_t *stream = NET0_newStream(buffer, sizeof(buffer));

    for(size_t i = 0; i < packedSize - 1; i++)
    {
        TEST_ASSERT_EQUAL(0, NET0_streamAddByte(stream, packedData[i]));
    }
    TEST_ASSERT_EQUAL(sizeof(data), 
                      NET0_streamAddByte(stream, packedData[packedSize - 1]));
    TEST_ASSERT_EQUAL_MEMORY(buffer, data, sizeof(data));
}

//+--------------------+
//| NET0_streamComplete |
//+====================+

// cppcheck-suppress unusedFunction 
void test_NET0_streamComplete_OnlyStxEtx(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x03);

    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamComplete_NewFrameBuff_should_ReturnFalse(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamComplete_Null_should_ReturnFalse(void)
{
    TEST_ASSERT_FALSE(NET0_streamComplete(NULL));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamComplete_OnlySTXFrame_should_ReturnFalse(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    NET0_streamAddByte(stream, 0x02);

    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
}

// cppcheck-suppress unusedFunction 
void test_NET0_streamComplete_CompleteFrame_should_ReturnTrue(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x04);
    NET0_streamAddByte(stream, 0x03);

    TEST_ASSERT_TRUE(NET0_streamComplete(stream));
}

//+---------------------+
//| NET0_clearStream |
//+=====================+

// cppcheck-suppress unusedFunction 
void test_NET0_clearStream_NULL(void)
{
    NET0_clearStream(NULL);
}

// cppcheck-suppress unusedFunction 
void test_NET0_clearStream_EmptyFrame_should_RemainEmpty(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
    NET0_clearStream(stream);
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
}

// cppcheck-suppress unusedFunction 
void test_NET0_clearStream_CompleteFrame_should_BecameEmpty(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x04);
    NET0_streamAddByte(stream, 0x03);
    TEST_ASSERT_TRUE(NET0_streamComplete(stream));
    NET0_clearStream(stream);
    TEST_ASSERT_FALSE(NET0_streamComplete(stream));
}

//+--------------+
//| NET0_streamPackedSize |
//+--------------+

// cppcheck-suppress unusedFunction 
void test_NET0_streamPackedSize(void)
{
    uint8_t data[10] = {0};
    NET0_stream_t *stream = NET0_newStream(data, sizeof(data));
    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x04);
    NET0_streamAddByte(stream, 0x03);
    TEST_ASSERT_EQUAL(3, NET0_streamPackedSize(stream));
    NET0_clearStream(stream);

    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x04);
    NET0_streamAddByte(stream, 0x03);
    TEST_ASSERT_EQUAL(3, NET0_streamPackedSize(stream));
    NET0_clearStream(stream);

    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x04);
    NET0_streamAddByte(stream, 0x05);
    NET0_streamAddByte(stream, 0x06);
    NET0_streamAddByte(stream, 0x07);
    NET0_streamAddByte(stream, 0x03);
    TEST_ASSERT_EQUAL(6, NET0_streamPackedSize(stream));
    NET0_clearStream(stream);

    NET0_streamAddByte(stream, 0x02);
    NET0_streamAddByte(stream, 0x10);
    NET0_streamAddByte(stream, 0x82);
    NET0_streamAddByte(stream, 0x10);
    NET0_streamAddByte(stream, 0x82);
    NET0_streamAddByte(stream, 0x03);
    TEST_ASSERT_EQUAL(6, NET0_streamPackedSize(stream));
    NET0_clearStream(stream);
}
// cppcheck-suppress unusedFunction 
void suiteSetUp(void)
{
}

// cppcheck-suppress unusedFunction 
int suiteTearDown(int num_failures)
{
    (void)num_failures;
    return 0;
}

// cppcheck-suppress unusedFunction 
void setUp(void)
{
    NET0_deinit();
}

// cppcheck-suppress unusedFunction 
void tearDown(void)
{
}

