#include "net0.h"
#include <stdio.h>


#define NELEMS(array) (sizeof(array) / sizeof(array[0]))
void print_array(uint8_t *array, int nelems)
{
    for(int i = 0; i < nelems; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main(void)
{
    uint8_t data[5] = {4, 5, 6, 7, 0x10};
    uint8_t unpackedData[5] = {0};
    uint8_t packedData[10] = {0};

    //pack
    size_t packedSize = NET0_pack(data, packedData, sizeof(data), sizeof(packedData));
    printf("Pack = %zu\r\n", packedSize);

    print_array(data, sizeof(data));
    print_array(packedData, sizeof(packedData));
    
    //unpack
    size_t unpackedSize = NET0_unpack(packedData, unpackedData, packedSize, sizeof(unpackedData));
    printf("Unpack = %zu\r\n", unpackedSize);
    print_array(unpackedData, sizeof(unpackedData));
    return 0;
}
